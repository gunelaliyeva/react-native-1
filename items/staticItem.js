import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Colors, width} from "../styles";
import {CustomText} from "./customText";
export const StaticItem = ({name,amount,type}) =>{
  return(
      <View style={style.container}>
          <CustomText>{name}</CustomText>
          <CustomText>x{amount} {type}</CustomText>
      </View>
  )
};

const style= StyleSheet.create({
    container:{
        width: width*0.9,
        height: 40,
        borderRadius: 25,
        borderWidth: 2,
        borderColor: Colors.lightOrange,
        paddingHorizontal: 20,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginBottom: 15,
    }
});