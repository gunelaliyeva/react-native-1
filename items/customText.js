import React from 'react';
import {Text} from 'react-native';
const FontFamilies={
  regular: "MontSerratRegular",
  medium: "MontSerratMedium",
  bold: "MontSerratBold"
};
export const CustomText =  ({weight,style,children,...rest}) =>{
  return  <Text style={{...style, fontFamily: FontFamilies[weight] || FontFamilies.regular}} {...rest}>{children}</Text>
};
