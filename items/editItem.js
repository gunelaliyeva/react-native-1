import React from 'react';
import {TouchableOpacity, View, StyleSheet,Image} from 'react-native';
import {Colors, width} from "../styles";
import {CustomText} from "./customText";
export const EditItem= ({name,amount,type}) =>{
  return(
      <View style={style.container}>
          <View style={style.item}>
              <TouchableOpacity >
                  <View style={{...style.edit, backgroundColor:Colors.lightOrange, borderColor: Colors.lightOrange}}>
                      <Image source={require('../styles/images/pen.png')}/>
                  </View>
              </TouchableOpacity>
              <CustomText style={{marginLeft:20}}>
                  {name}
              </CustomText>
          </View>
          <View style={style.item}>
              <CustomText style={{marginRight:20}}>
                  x{amount} {type}
              </CustomText>
              <TouchableOpacity >
                  <View style={{...style.edit, backgroundColor:Colors.vermillon, borderColor: Colors.vermillon}}>
                      <Image source={require('../styles/images/cancel.png')}/>
                  </View>
               </TouchableOpacity>
          </View>
      </View>
  )
};

const style=StyleSheet.create({
    container:{
        width:width*0.9,
        borderColor: Colors.lightOrange,
        borderWidth: 2,
        borderRadius: 25,
        height: 40,
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: 15
    },
    edit:{
        width:40,
        height: 40,
        borderRadius: 25,
        overflow: "hidden",
        justifyContent: "center",
        alignItems: "center",
        borderWidth: 2,
    },
    item:{
        flexDirection: "row",
        alignItems: "center"
    }
});