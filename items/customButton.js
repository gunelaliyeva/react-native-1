import React from 'react';
import {View} from 'react-native';
import {CustomText} from "./customText";
export const CustomButton = ({weight,style,text,children,...rest}) =>{
  return(
      <View style={style} {...rest} >
          <CustomText style={text} weight={weight}>{children}</CustomText>
      </View>
  )
};