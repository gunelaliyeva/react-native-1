import React from 'react';
import {View,StyleSheet,Image,TouchableOpacity,Text} from 'react-native';
import {DrawerContentScrollView} from '@react-navigation/drawer';
import {CustomText} from "./customText";
import {Colors, height, screenStyle} from "../styles";
import store from "../store";
export const CustomDrawer = ({navigation}) =>{
    const url=store.getState().userInfo.imgUrl===""?"C:\\Users\\Asus\\Desktop\\React-Native-Home-Works\\HomeWork1\\Homework1\\styles\\images\\profileAvatar.jpg":
        store.getState().userInfo.imgUrl;
  return(
      <View  style={style.container}>
          <DrawerContentScrollView >
              <View style={style.headerContainer}>
                  <View style={style.imgContainer}></View>
                  <CustomText style={{color: "gray", fontSize: 24}} weight={"bold"}>{store.getState().userInfo.name}</CustomText>
              </View>
              <View style={{...screenStyle.body,backgroundColor: Colors.vermillon, height: height*0.9, marginTop: 0}}>
                  <TouchableOpacity
                      onPress={()=>{navigation.navigate('AddNewList');
                          navigation.closeDrawer()}}>
                      <View style={{...style.button,marginBottom:35}} ><CustomText style={style.text} weight={"bold"}>add new list</CustomText>
                      </View></TouchableOpacity>
                  <TouchableOpacity onPress={()=>{navigation.navigate('OneTimeList');
                      navigation.closeDrawer()}}>
                      <View style={style.button}><CustomText style={style.text} weight={"bold"}>one time list</CustomText></View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={()=>{navigation.navigate('RegularList');
                      navigation.closeDrawer()}}>
                      <View style={style.button}><CustomText style={style.text} weight={"bold"}>regular list</CustomText></View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={()=>{navigation.navigate('UserSettings');
                      navigation.closeDrawer()}}>
                      <View style={style.button}><CustomText style={style.text} weight={"bold"}>user settings</CustomText></View>
                  </TouchableOpacity>
              </View>
          </DrawerContentScrollView>
      </View>
  )
};

const style=StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: "white",
    },
    text:{
        color: Colors.vermillon,
        fontSize: 14,
        textTransform: "uppercase",
    },
    button:{
        backgroundColor: "white",
        width: 250,
        height: 35,
        borderRadius: 40,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 10,
    },
    headerContainer:{
        alignItems: "center",
        //paddingTop: height*0.02,
        padding: 10,
        flexDirection: "row",
        marginHorizontal: 20,
    },
    imgContainer:{
        width: 50,
        height: 50,
        borderColor: Colors.vermillon,
        borderWidth: 2,
        borderRadius: 30,
        marginRight: 20,
    },
    image:{
        resizeMode: "contain",
    }
});