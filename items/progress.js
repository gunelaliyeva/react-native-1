import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Colors, width} from "../styles";
import {CustomText} from "./customText";
export const ProgressItem = ({title,completed,allCount}) =>{
  return(

      <View style={style.container}>
          <View style={style.textContainer}>
              <CustomText style={style.text}>{title}</CustomText>
              <CustomText style={style.text}>{completed}/{allCount}</CustomText>
          </View>
          <View style={style.containerStyles}>
              <View style={{...style.fillerStyles,width: width*0.9*(allCount===0?0:completed/allCount)}}>
              </View>
          </View>
      </View>
  )
};

const style =StyleSheet.create({
    container:{
        width: width*0.9,
        borderColor: Colors.lightOrange,
        borderWidth: 2,
        borderRadius: 10,
        height: 75,
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginBottom: 15,
    },
    textContainer:{
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: 10,
    },
    containerStyles: {
        height: 20,
        backgroundColor: Colors.lightGray,
        borderRadius: 20,
        width: width*0.9-40,
    },

    fillerStyles: {
        height: 20,
        backgroundColor: Colors.lightOrange,
        borderRadius: 20,
        textAlign: 'right',
    },
});