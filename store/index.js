import {createStore,combineReducers,applyMiddleware} from 'redux';
import {projectReducer} from "./project";
/*
const MainReducer=combineReducers({
    [name]: reducer,
});

 */
const store = createStore(projectReducer,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;