const ADD_REGULAR_LIST="ADD_REGULAR_LIST";
const EDIT_REGULAR_LIST="EDIT_REGULAR_LIST";
const EDIT_ONE_TIME_LIST="EDIT_ONE_TIME_LIST";
const ADD_ONE_TIME_LIST="ADD_ONE_TIME_LIST";
const DELETE_ONE_TIME_LIST="DELETE_ONE_TIME_LIST";
const DELETE_REGULAR_LIST="DELETE_REGULAR_LIST";
const CREATE_ONE_TIME_LIST= "CREATE_ONE_TIME_LIST";
const CREATE_REGULAR_LIST="CREATE_REGULAR_LIST";
const SET_USERINFO="SET_USERINFO";
const SET_SELECTED="SET_SELECTED";
const SET_NEW_LIST="SET_NEW_LIST";



const initialState= {
    userInfo: {
        name: "gunel",
        imgUrl: ""
    },
    selected:{
        listName: "",
        listItem: "",
        listType: "",
    },
    newList:{
      title: "",
      completed:0,
      allCount: 0,
      items: []
    },
    oneTimeList: [
             {
                title: "for breakfast",
                completed: 1,
                allCount: 2,
                items: [
                    {
                        name: "breakfast",
                        count: 2,
                        type: "litre"
                    },
                    {
                        name: "home",
                        count: 7,
                        type: "bott"
                    }
                ]
            },
            {
                title: "home",
                completed: 3,
                allCount: 7,
                items:[
                    {
                        name: "breakfast",
                        count: 2,
                        type: "litre"
                    },
                    {
                        name: "home",
                        count: 7,
                        type: "bott"
                    }
                ]
            }
            ],
    regularList: [
        {
            title: "for breakfast",
            completed: 1,
            allCount: 2,
            items: [
                {
                    name: "breakfast",
                    count: 2,
                    type: "pgk"
                },
                {
                    name: "home",
                    count: 7,
                    type: "kg"
                }
            ]
        },
        {
            title: "home",
            completed: 3,
            allCount: 7,
            items:[
                {
                    name: "breakfast",
                    count: 2,
                    type: "pkg"
                },
                {
                    name: "home",
                    count: 7,
                    type: "litre"
                }
            ]
        }
    ]
    };

export function projectReducer(state=initialState,{type,payload}) {
    switch (type) {
        case SET_USERINFO:
            return {
              ...state,
                userInfo: payload,
            };
        case ADD_ONE_TIME_LIST:
            return {
                ...state,
                oneTimeList: [...state.oneTimeList,payload]
            };
        case ADD_REGULAR_LIST:
            return {
                ...state,
                regularList: [...state.regularList,payload]
            };
        case CREATE_ONE_TIME_LIST:
            return {
              ...state,
              oneTimeList: [...state.oneTimeList,{title: payload.title,completed: 0,allCount: 0,items: []}]
            };
        case CREATE_REGULAR_LIST:
            return {
            ...state,
            regularList: [...state.regularList,{title: payload.title,completed: 0,allCount: 0,items: []}]
        };
        case SET_SELECTED:
            return {
                ...state,
                selected: {...state.selected,...payload}
            };
        case SET_NEW_LIST:
            return{
                ...state,
                newList: {...state.newList,...payload},
        };
        case DELETE_REGULAR_LIST:
            return {
              ...state,
              regularList: state.regularList.filter(item=>item.title!=payload)
            };
        default: return state;
    }
}

export const setNewList=(payload) =>({
    type: SET_NEW_LIST,payload
});


export const setSelected=(payload) =>({
    type: SET_SELECTED,payload
});

export const addRegularList=(payload) =>({
    type: ADD_REGULAR_LIST, payload
});
export const addOneTimeList=(payload) =>({
    type: ADD_ONE_TIME_LIST, payload
});
export const editRegularList=(payload) =>({
    type: EDIT_REGULAR_LIST, payload
});
export const deleteRegularList=(payload) =>({
    type: DELETE_REGULAR_LIST, payload
});
export const editOneTimeList=(payload) =>({
    type: EDIT_ONE_TIME_LIST, payload
});
export const deleteOneTimeList=(payload) =>({
    type: DELETE_ONE_TIME_LIST, payload
});

export const updateUserInfo = (payload) =>({
    type: SET_USERINFO,payload
});

export const createRegularList = (payload) =>({
    type: CREATE_REGULAR_LIST,payload
});
export const createOneTimeList = (payload) =>({
   type:  CREATE_ONE_TIME_LIST,payload
});