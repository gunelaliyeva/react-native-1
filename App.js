import React ,{useState}from 'react';
import {MainNavigation} from "./navigation";
import {AppLoading} from 'expo'
import {loadFonts} from './styles/fonts/font'
import {Provider} from 'react-redux';
import store from "./store";
export default function App() {
  const [loaded,setLoaded]= useState(false);
  if(!loaded)
  {
    return <AppLoading
        startAsync={loadFonts}
        onFinish={()=>setLoaded(true)}/>
  }
  return(
      <Provider store={store}>
        <MainNavigation/>
      </Provider>
  )
}

/*
const [loaded,setLoaded]= useState(false);
  if(!loaded)
  {
    return <AppLoading
    startAsync={loadFonts}
    onFinish={()=>setLoaded}/>
  }
 */
