import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {UserSettings} from "../screens";
const {Navigator,Screen} =createStackNavigator();
export const UserSettingsStack = () =>
    (
        <Navigator headerMode={"none"}>
            <Screen name={"UserSettings"} component={UserSettings}/>
        </Navigator>
    );
