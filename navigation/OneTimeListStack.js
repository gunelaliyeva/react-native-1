import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {HomePage1, SingleItemEdit, SingleListEdit, SingleListStatic} from "../screens";
const {Navigator,Screen} =createStackNavigator();
export const OneTimeListStack = ({navigation}) =>
    (
        <Navigator headerMode={"none"} >
            <Screen name={"OneTimeList"} component={HomePage1} options={{gestureEnabled:false}}/>
            <Screen name={"SingleListEdit"} component={SingleListEdit} options={{gestureEnabled:false}}/>
            <Screen name={"SingleItemEdit"} component={SingleItemEdit} options={{gestureEnabled:false}}/>
            <Screen name={"SingleListStatic"} component={SingleListStatic} options={{gestureEnabled:false}}/>
        </Navigator>
    );
