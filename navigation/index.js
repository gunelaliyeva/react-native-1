import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import {AddNewListStack} from "./AddNewListStack";
import {OneTimeListStack} from "./OneTimeListStack";
import {RegularListStack} from "./RegularListStack";
import {UserSettingsStack} from "./UsersettingsStack";
import {CustomDrawer} from "../items";
const {Navigator,Screen} =createDrawerNavigator();

export const MainNavigation = () => (
    <NavigationContainer >
        <Navigator drawerContent={CustomDrawer}>
            <Screen name="AddNewList" component={AddNewListStack} />
            <Screen name={"OneTimeList"} component={OneTimeListStack}/>
            <Screen name={"RegularList"} component={RegularListStack} />
            <Screen name={"UserSettings"} component={UserSettingsStack}/>
        </Navigator>
    </NavigationContainer>
);

