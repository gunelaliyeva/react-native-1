import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {AddNewList, HomePage1, SingleListEdit, SingleListStatic} from "../screens";
const {Navigator,Screen} =createStackNavigator();
export const AddNewListStack = () =>
    (
        <Navigator headerMode={"none"}>
            <Screen name="AddNewList" component={AddNewList}/>
            <Screen name={"SingleListEdit"} component={SingleListEdit}/>
            <Screen name={"SingleListStatic"} component={SingleListStatic}/>
            <Screen name={"OneTimeList"} component={HomePage1}/>
        </Navigator>
    );
