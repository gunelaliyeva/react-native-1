import React,{useState} from 'react';
import uuid from 'react-uuid';
import {View, Image,TouchableOpacity, StyleSheet} from 'react-native';
import {Colors, screenStyle, width} from "../../styles";
import {StaticItem} from "../../items/staticItem";
import {CustomButton, CustomText} from "../../items";
import store from "../../store";
export const SingleListStatic = ({navigation}) => {
    const name=store.getState().selected.listName;
    let items=[];
    store.getState().selected.listType==="regular"?
    store.getState().regularList.forEach(item=>item.title==name? items=item.items:console.log(item.title)):
        store.getState().oneTimeList.forEach(item=>item.title==name? items=item.items:console.log(item.title));
    return(
        <View style={screenStyle.container} >
            <View style={screenStyle.headerContainer}>
                <TouchableOpacity onPress={()=>navigation.navigate("OneTimeList")}>
                <Image source={require('../../styles/images/back.png')}/>
                </TouchableOpacity>
                <CustomText style={screenStyle.headerText} weight={"bold"} >
                    {name}
                </CustomText>
                <TouchableOpacity onPress={()=>navigation.navigate("SingleItemEdit")}>
                <Image source={require('../../styles/images/pen.png')}/>
                </TouchableOpacity>
            </View>
            <View style={screenStyle.body}>
                <View style={topSideStyle.container}>
                    <TouchableOpacity>
                        <CustomButton style={topSideStyle.button} text={topSideStyle.buttonText} weight={"bold"}>Reset</CustomButton>
                    </TouchableOpacity>
                    <CustomText>{items.length}</CustomText>
                </View>
                {items.map(item=><TouchableOpacity key={uuid()}><StaticItem name={item.name} amount={item.count} type={item.type} key={uuid()}/></TouchableOpacity>)}
            </View>
        </View>
    )
};

const topSideStyle = StyleSheet.create({
   container:{
       flexDirection: "row",
       width: width*0.9,
       justifyContent: "space-between",
       alignItems: "center",
       marginBottom: 15,
   },
    button: {
       backgroundColor: Colors.vermillon,
        width: 70,
        height: 20,
        borderRadius: 50,
        justifyContent: "center",
        alignItems: "center"
    },
    buttonText:{
        color: "white",
        fontSize: 10,
        textTransform: "uppercase",
    }
});