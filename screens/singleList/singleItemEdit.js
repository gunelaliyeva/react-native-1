import React from 'react';
import {View, Image, TouchableOpacity, TextInput, StyleSheet,TouchableWithoutFeedback,Keyboard} from 'react-native';
import {Colors, screenStyle, width} from "../../styles";
import {CustomButton, CustomText, EditItem} from "../../items";
import store from "../../store";
import uuid from 'react-uuid'
export const SingleItemEdit = ({navigation}) => {
    const name=store.getState().selected.listName;
    let items=[];
    store.getState().selected.listType==="regular"?
        store.getState().regularList.forEach(item=>item.title==name? items=item.items:console.log(item.title)):
        store.getState().oneTimeList.forEach(item=>item.title==name? items=item.items:console.log(item.title));
    return(
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={screenStyle.container}>
            <View style={screenStyle.headerContainer}>
                <TouchableOpacity onPress={()=>navigation.navigate('OneTimeList')}>
                <Image source={require('../../styles/images/back.png')}/>
                </TouchableOpacity>
                <CustomText style={screenStyle.headerText} weight={"bold"}>
                    {name}
                </CustomText>
                <TouchableOpacity onPress={()=>navigation.navigate('SingleListStatic')}>
                <Image source={require('../../styles/images/save.png')}/>
                </TouchableOpacity>
            </View>
            <View style={screenStyle.body}>
                <View style={style.headContainer}>
                    <View style={style.textInputContainer}>
                        <CustomText>position name</CustomText>
                        <TextInput style={style.input}/>
                    </View>
                    <View>
                        <CustomText>count</CustomText>
                        <View style={style.count}>
                            <TouchableOpacity>
                                <CustomButton style={{...style.countBtn, borderTopLeftRadius:45,borderBottomLeftRadius:45,}} weight={"bold"}>-</CustomButton>
                            </TouchableOpacity>
                            <View style={{...style.countBtn,width:width*0.1}}>
                                <CustomText weight={"bold"}>count</CustomText>
                            </View>
                            <TouchableOpacity>
                                <CustomButton style={{...style.countBtn, borderTopRightRadius:45,borderBottomRightRadius:45}} weight={"bold"}>+</CustomButton>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={style.buttonContainer}>
                    <TouchableOpacity><CustomButton style={style.measureButton} weight={"bold"}>pkg</CustomButton></TouchableOpacity>
                    <TouchableOpacity><CustomButton style={style.measureButton} weight={"bold"}>kg</CustomButton></TouchableOpacity>
                    <TouchableOpacity><CustomButton style={style.measureButton} weight={"bold"}>litre</CustomButton></TouchableOpacity>
                    <TouchableOpacity><CustomButton style={style.measureButton} weight={"bold"}>bott</CustomButton></TouchableOpacity>
                </View>
                <View style={style.buttons}>
                <TouchableOpacity>
                    <CustomButton style={style.addBtn} text={{color:"white",textTransform: "uppercase",fontSize:14}} weight={"bold"}>cancel</CustomButton>
                </TouchableOpacity>
                <TouchableOpacity>
                    <CustomButton style={style.addBtn} text={{color:"white",textTransform: "uppercase",fontSize:14}} weight={"bold"}>update</CustomButton>
                </TouchableOpacity>
                </View>
                <View style={style.line}></View>
                {items.map(item=><TouchableOpacity key={uuid()}><EditItem name={item.name} amount={item.count} type={item.type}/></TouchableOpacity>)}
            </View>
        </View>
        </TouchableWithoutFeedback>
    )
};

const style=StyleSheet.create({
    headContainer:{
        flexDirection: "row",
        width: width*0.9,
        justifyContent: "space-between",
        marginBottom: 20,
    },
    textInputContainer:{
        alignItems:"center",
        justifyContent: "center",
    },
    input:{
        backgroundColor: Colors.lightGray,
        width: width*0.6,
        height: 40,
        borderRadius: 45,
        fontWeight: "bold",
        paddingHorizontal:20,
        textAlign: "center",
    },
    count:{
        flexDirection: "row",
    },
    countBtn:{
        backgroundColor: Colors.lightGray,
        width: width*0.07,
        height: 40,
        justifyContent: "center",
        alignItems: "center",
    },
    buttonContainer: {
        flexDirection: "row",
        width: width*0.9,
        justifyContent: "space-between",
        marginBottom: 20,
    },
    measureButton:{
        backgroundColor: Colors.lightGray,
        justifyContent: "center",
        alignItems: "center",
        width: width*0.2,
        height: 40,
        borderRadius: 45,
    },
    addBtn:{
        width: width*0.4,
        backgroundColor: Colors.vermillon,
        height:40,
        borderRadius: 45,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 20,
    },
    line:{
        width:width,
        height:2,
        backgroundColor:Colors.lightGray,
        marginBottom: 30,
    },
    buttons:{
        flexDirection: "row",
        width: width*0.9,
        justifyContent: "space-between",
    }
});