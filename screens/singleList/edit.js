import React,{useState} from 'react';
import {View,Image,TouchableOpacity,TextInput,StyleSheet,Keyboard,TouchableWithoutFeedback,ScrollView} from 'react-native';
import {Colors, screenStyle, width} from "../../styles";
import {CustomButton, CustomText, EditItem} from "../../items";
import store from "../../store";
import uuid from 'react-uuid';
import {addToRegularList, addToOneTimeList, addOneTimeList, setNewList, addRegularList} from '../../store/project';
export const SingleListEdit = ({navigation}) => {
   const listName=store.getState().selected.listName;
   const listType=store.getState().selected.listType;
   const [name,setName]=useState("");//item's name
    const [count,setCount]=useState(0); //item's count
    const [type,setType]=useState(""); //item's measure type
    const obj=store.getState().newList; //new list object for adding
    const newList={
        title:obj.title,
    completed: obj.completed,
    allCount: obj.allCount,
    items:obj.items};
   let items=[...store.getState().newList.items];
   const addListHandler=(item)=>{
       store.dispatch(setNewList({title: listName,
       completed: 0,
       allCount: store.getState().newList.allCount+1,
       items:[...store.getState().newList.items,item]}));
   };
   const save = () =>{
       listType!=="regular"?
     addOneTimeList(newList):
     addRegularList(newList);
   };
    return(
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={screenStyle.container}>
            <View style={screenStyle.headerContainer}>
                <TouchableOpacity onPress={()=>navigation.navigate('OneTimeList')}>
                <Image source={require('../../styles/images/back.png')} />
                </TouchableOpacity>
                <CustomText style={screenStyle.headerText} weight={"bold"}>
                    {listName}
                </CustomText>
                <TouchableOpacity onPress={() =>{navigation.navigate('SingleListStatic');
                save()}}>
                <Image source={require('../../styles/images/save.png')}/>
                </TouchableOpacity>
            </View>
            <View style={screenStyle.body}>
                <View style={style.headContainer}>
                    <View style={style.textInputContainer}>
                        <CustomText>position name</CustomText>
                        <TextInput style={style.input} onChangeText={val=>setName(val)}/>
                    </View>
                    <View style={style.textInputContainer}>
                        <CustomText>count</CustomText>
                        <View style={style.count}>
                            <TouchableOpacity onPress={()=>setCount(count-1)}>
                            <CustomButton style={{...style.countBtn, borderTopLeftRadius:45,borderBottomLeftRadius:45,}} weight={"bold"}>-</CustomButton>
                            </TouchableOpacity>
                            <View style={{...style.countBtn,width:width*0.1}}>
                                <CustomText weight={"bold"}>{count}</CustomText>
                            </View>
                            <TouchableOpacity onPress={()=>setCount(count+1)}>
                            <CustomButton style={{...style.countBtn, borderTopRightRadius:45,borderBottomRightRadius:45}} weight={"bold"}>+</CustomButton>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={style.buttonContainer}>
                    <TouchableOpacity disabled={type==="pkg"?true:false} onPress={()=>setType("pkg")} ><CustomButton style={{...style.measureButton, opacity:type==="pkg"?0.5:1}} weight={"bold"}>pkg</CustomButton></TouchableOpacity>
                    <TouchableOpacity disabled={type==="kg"?true:false} onPress={()=>setType("kg")}><CustomButton style={{...style.measureButton, opacity:type==="kg"?0.5:1}} weight={"bold"}>kg</CustomButton></TouchableOpacity>
                    <TouchableOpacity disabled={type==="litre"?true:false} onPress={()=>setType("litre")}><CustomButton style={{...style.measureButton, opacity:type==="litre"?0.5:1}} weight={"bold"}>litre</CustomButton></TouchableOpacity>
                    <TouchableOpacity disabled={type==="bott"?true:false} onPress={()=>setType("bott")}><CustomButton style={{...style.measureButton, opacity:type==="bott"?0.5:1}} weight={"bold"}>bott</CustomButton></TouchableOpacity>
                </View>
                <TouchableOpacity onPress={()=>addListHandler({name: name,
                count: count, type: type})}>
                <CustomButton style={style.addBtn} text={{color:"white",textTransform: "uppercase",fontSize:14}} weight={"bold"}>add to list</CustomButton>
                </TouchableOpacity>
                <View style={style.line}></View>
                <ScrollView>
                {
                    items.map(item=><TouchableOpacity key={uuid()}><EditItem name={name} type={type} amount={count}/></TouchableOpacity>)
                }
                </ScrollView>
            </View>
        </View>
        </TouchableWithoutFeedback>
    )
};

const style=StyleSheet.create({
    headContainer:{
        flexDirection: "row",
        width: width*0.9,
        justifyContent: "space-between",
        marginBottom: 20,
    },
    textInputContainer:{
        alignItems:"center",
        justifyContent: "center",
    },
    input:{
        backgroundColor: Colors.lightGray,
        width: width*0.6,
        height: 40,
        borderRadius: 45,
        fontWeight: "bold",
        paddingHorizontal:20,
        textAlign: "center",
    },
    count:{
        flexDirection: "row",
    },
    countBtn:{
        backgroundColor: Colors.lightGray,
        width: width*0.07,
        height: 40,
        justifyContent: "center",
        alignItems: "center",
    },
    buttonContainer: {
        flexDirection: "row",
        width: width*0.9,
        justifyContent: "space-between",
        marginBottom: 20,
    },
    measureButton:{
        backgroundColor: Colors.lightGray,
        justifyContent: "center",
        alignItems: "center",
        width: width*0.2,
        height: 40,
        borderRadius: 45,
    },
    addBtn:{
        width: width*0.9,
        backgroundColor: Colors.vermillon,
        height:40,
        borderRadius: 45,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 20,
    },
    line:{
        width:width,
        height:2,
        backgroundColor:Colors.lightGray,
        marginBottom: 30,
    }
});