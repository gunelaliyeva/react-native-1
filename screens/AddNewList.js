import React ,{useState,useEffect} from 'react';
import {View, TextInput, Keyboard, TouchableWithoutFeedback, TouchableOpacity, StyleSheet} from 'react-native';
import {Colors, screenStyle, width} from "../styles";
import {bigTextInput, saveButton} from "../styles";
import {CustomButton, CustomText} from "../items";
import store from "../store";
import {createOneTimeList, createRegularList, setNewList, setSelected} from "../store/project";
export const AddNewList = ({navigation}) => {
    const [listName,setListName]=useState("");
    const [type,setType]=useState("oneTime");
    const addListHandler = () =>{
        store.dispatch(setSelected({listName: listName,listType: type}));
        navigation.navigate('SingleListEdit')
    };
    return(
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={screenStyle.container} >
            <View style={{...screenStyle.headerContainer,justifyContent: "center"}}>
                <CustomText style={screenStyle.headerText} weight={"bold"}>
                    New List
                </CustomText>
            </View>
            <View style={screenStyle.body}>
                <CustomText style={{fontSize:12}}>list name</CustomText>
                <TextInput style={bigTextInput.item} onChangeText={text=>setListName(text)} value={listName}/>
                <View style={style.buttonContainer}>
                    <TouchableOpacity disabled={type==="oneTime"?true:false} onPress={()=>setType("oneTime")}>
                    <CustomButton style={{...style.choiceButton,opacity:type==="oneTime"?0.5:1}} weight={type==="regular"?"bold":"regular"} >one time</CustomButton>
                    </TouchableOpacity>
                    <TouchableOpacity disabled={type==="regular"?true:false} onPress={()=>setType("regular")}>
                    <CustomButton style={{...style.choiceButton,opacity:type==="regular"?0.5:1}} weight={type==="oneTime"?"bold":"regular"}>regular</CustomButton>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={saveButton.button}
                                  onPress={addListHandler}>
                    <CustomButton  color="white" text={style.buttonText} style={style.button} weight={"bold"}
                                   >create list</CustomButton>
                </TouchableOpacity>
            </View>
        </View>
        </TouchableWithoutFeedback>
    )
};

const style=StyleSheet.create({
    button:{
        width:width*0.9,
        backgroundColor: Colors.vermillon,
        justifyContent: "center",
        alignItems: "center",
        height: 40,
        borderRadius: 40,
    },
    buttonText:{
        fontSize:14,
        color: "white",
        textTransform: "uppercase",
    },
    buttonContainer:{
        flexDirection: "row",
        justifyContent: "space-between",
        width: width*0.9,
    },
    choiceButton:{
        width: width*0.4,
        backgroundColor: Colors.lightGray,
        height: 40,
        borderRadius: 45,
        justifyContent: "center",
        alignItems: "center",
    }
});