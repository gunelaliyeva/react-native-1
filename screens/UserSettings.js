import React,{useState} from 'react';
import {View, TextInput,TouchableWithoutFeedback,Keyboard,StyleSheet,TouchableOpacity} from 'react-native';
import {bigTextInput, Colors, screenStyle, width} from "../styles";
import {CustomButton, CustomText} from "../items";
import store from "../store";
import {updateUserInfo} from "../store/project";
export const UserSettings = () => {
    const [userName,setUserName]=useState(store.getState().userInfo.name);
    const [image,setImage]=useState(store.getState().userInfo.imgUrl);
    const saveChanges = () =>{
        store.dispatch(updateUserInfo({name: userName,imgUrl: image}))
    };
    store.subscribe(()=>console.log(store.getState()));
    return(
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={screenStyle.container}>
            <View style={{...screenStyle.headerContainer,justifyContent: "center"}}>
                <CustomText style={screenStyle.headerText} weight={"bold"}>
                    User Settings
                </CustomText>
            </View>
            <View style={screenStyle.body}>
                <CustomText style={{fontSize:12}}>username</CustomText>
                <TextInput style={bigTextInput.item} onChangeText={text=>setUserName(text)} value={userName}/>
                <CustomText style={{fontSize:12}}>avatar url</CustomText>
                <TextInput style={bigTextInput.item} onChangeText={text=>setImage(text)} value={image}/>
                <TouchableOpacity onPress={saveChanges}>
                    <CustomButton style={style.button} weight={"bold"} text={style.buttonText}>save changes</CustomButton>
                </TouchableOpacity>
            </View>
        </View>
        </TouchableWithoutFeedback>
    )
};

const style=StyleSheet.create({
    button:{
        width:width*0.9,
        backgroundColor: Colors.vermillon,
        justifyContent: "center",
        alignItems: "center",
        height: 40,
        borderRadius: 40,
    },
    buttonText:{
        fontSize:14,
        color: "white",
        textTransform: "uppercase",
    }
});