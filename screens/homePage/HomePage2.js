import React from 'react';
import uuid from 'react-uuid';
import {View,Image,TouchableOpacity,Alert,ScrollView} from 'react-native';
import {screenStyle} from "../../styles";
import {CustomText, ProgressItem} from "../../items";
import store from "../../store";
import {deleteRegularList, setSelected} from "../../store/project";
export const HomePage2 = ({navigation}) => {
    const presshandler= (item) =>
    {navigation.navigate('SingleListStatic');
        store.dispatch(setSelected({listName:item.title,listType: "regular"}))};
    const deleteHandler = (title) =>
        Alert.alert(
            "Do you want to delete this list?",
            "If you press yes, it will be deleted",
            [
                {
                    text: "Cancel",
                    onPress: ()=>{},
                    style: "cancel"
                },
                {
                    text: "Yes, Delete",
                    onPress: () =>deleteRegularList(title)
                }
            ]
        );
    return(
        <View style={screenStyle.container}>
            <View style={{...screenStyle.headerContainer}}>
                <Image/>
                <CustomText style={screenStyle.headerText} weight={"bold"}>
                    Regular List
                </CustomText>
                <TouchableOpacity onPress={()=>navigation.openDrawer()}>
                <Image source={require('../../styles/images/menu.png')} />
                </TouchableOpacity>
            </View>
            <View style={screenStyle.body}>
                {
                    store.getState().regularList.map(item=>
                    <TouchableOpacity onPress={() => presshandler(item)} key={uuid()} onLongPress={()=>deleteHandler(item.title)}>
                        <ProgressItem title={item.title} completed={item.completed} allCount={item.allCount} key={uuid()}/>
                    </TouchableOpacity>)
                }
            </View>
        </View>
    )
};