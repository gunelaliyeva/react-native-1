import React,{useState} from 'react';
import uuid from 'react-uuid';
import {View,Image,TouchableOpacity} from 'react-native';
import {screenStyle} from "../../styles";
import {CustomText, ProgressItem} from "../../items";
import store from "../../store";
import {setSelected} from "../../store/project";
export const HomePage1 = ({navigation}) => {
    const pregresHandler=(item) =>{
        navigation.navigate('SingleListStatic');
        store.dispatch(setSelected({listName: item.title,listType: "oneTime"}));
    };
    return(
        <View style={screenStyle.container}>
            <View style={{...screenStyle.headerContainer}}>
                <Image/>
                <CustomText style={screenStyle.headerText} weight={"bold"}>
                    One Time List
                </CustomText>
                <TouchableOpacity onPress={()=>navigation.openDrawer()}>
                <Image source={require('../../styles/images/menu.png')}/>
                </TouchableOpacity>
            </View>
            <View style={screenStyle.body}>
                    {
                        store.getState().oneTimeList.map((item,index)=><TouchableOpacity onPress={()=>pregresHandler(item)} key={uuid()}>
                            <ProgressItem title={item.title} completed={item.completed} allCount={item.allCount}/></TouchableOpacity>)
                    }
            </View>
        </View>
    )
};

/*
store.getState().oneTimeList.map((item,index)=>{<ProgressItem key={index} title={item.list1.title} completed={item.list1.completed} allCount={item.list1.allCount}/>  })
 */