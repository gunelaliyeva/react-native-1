import * as Font from "expo-font";
import MontSerratBold from './Montserrat-Bold.ttf';
import MontSerratRegular from './Montserrat-Regular.ttf';
import MontSerratMedium from './Montserrat-Medium.ttf';

export const loadFonts= () =>{
  return Font.loadAsync({
      MontSerratBold,
      MontSerratRegular,
      MontSerratMedium
  });
};