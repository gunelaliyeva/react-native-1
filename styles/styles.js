import {StyleSheet,Dimensions} from "react-native";
import {Colors} from "./colors";
import {exp} from "react-native-reanimated";
export const width=Dimensions.get('window').width;
export const height=Dimensions.get('window').height;
export const screenStyle=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor: Colors.vermillon,
        textAlign: "center",
        color: "white",
    },
    body:{
        flex: 1,
        backgroundColor: "white",
        marginTop: height*0.05,
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        paddingVertical: 20,
        alignItems: "center",
    },
    headerText:{
        color: "white",
        fontSize: 18,
    },
    headerContainer:{
        alignItems: "center",
        paddingTop: height*0.04,
        flexDirection: "row",
        justifyContent: "space-between",
        marginHorizontal: 30,
    }
});
export const bigTextInput=StyleSheet.create({
   item:{
       backgroundColor: Colors.lightGray,
       height: 45,
       width: width*0.9,
       borderRadius: 45,
       marginVertical: 15,
       fontSize: 18,
       paddingHorizontal: 30,
   }
});
export const saveButton=StyleSheet.create({
    button:{
        backgroundColor: Colors.vermillon,
        height: 45,
        width: width*0.9,
        borderRadius: 45,
        marginVertical: 15,

    }
});