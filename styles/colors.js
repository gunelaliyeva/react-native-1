export const Colors={
    vermillon: "#FF7676",
    orange: "#FFD976",
    lightOrange: "#FFE194",
    lightGray: "#EEEEEE",
    dark: "#303234"
};